class SmithPoint:
    def to_impedance(self):
        pass

    def zReal(self):
        pass

    def yReal(self):
        pass

    def zIm(self):
        pass

    def yIm(self):
        pass

    def to_impedance(self):
        pass

    def to_admittance(self):
        pass

    def normalize(self, Z0 ):
        if isinstance(Z0, SmithPoint):
            return Impedance(self.to_impedance().Z/Z0.to_impedance().Z)
        elif isinstance(Z0,Z0 [float, int]):
            return Impedance(self.to_impedance().Z/Z0)

    def denormalize(self, Z0):
        if isinstance(Z0, SmithPoint):
            return Impedance(self.to_impedance().Z*Z0.to_impedance().Z)
        elif isinstance(Z0, Z0[float, int]):
            return Impedance(self.to_impedance().Z * Z0)

    def yVal(self):
        return self.to_admittance().Y

    def zVal(self):
        return self.to_impedance().Z


class Admittance(SmithPoint):
    def to_admittance(self):
        return self

    def zReal(self):
        return self.to_impedance().Z.real

    def yReal(self):
        return self.Y.real

    def yIm(self):
        return self.Y.imag

    def zIm(self):
        return self.to_impedance().Z.imag

    def __init__(self, Y:complex):
        self.Y = Y

    def __add__(self, other):
        if isinstance(other, Admittance):
            return Admittance(self.Y + other.Y)
        elif isinstance(other, Impedance):
            return Admittance(self.Y + other.to_admittance().Y)

    def __sub__(self, other):
        if isinstance(other, Admittance):
            return Admittance(self.Y - other.Y)
        elif isinstance(other, Impedance):
            return Admittance(self.Y - other.to_admittance().Y)

    def to_impedance(self):
        return Impedance(1/self.Y)


class Impedance(SmithPoint):
    def __init__(self, Z :complex):
        self.Z = Z

    def __add__(self, other):
        if isinstance(other, Admittance):
            return Impedance(self.Z + other.to_impedance().Z)
        elif isinstance(other, Impedance):
            return Impedance(self.Z + other.Z)

    def __sub__(self, other):
        if isinstance(other, Admittance):
            return Impedance(self.Z - other.to_impedance().Z)
        elif isinstance(other, Impedance):
            return Impedance(self.Z - other.Z)

    def to_admittance(self):
        return Admittance(1 / self.Z)

    def zReal(self):
        return self.Z.real

    def yReal(self):
        return self.to_admittance().yReal()

    def yIm(self):
        return self.to_admittance().yIm()

    def zIm(self):
        return self.Z.imag

    def to_impedance(self):
        return self




