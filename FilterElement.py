from enum import Enum
from math import pi

from scipy.constants.constants import speed_of_light


class FilterElementType(Enum):
    L = 1
    C = 2
    CLs = 3
    CLp = 4

class FilterElement:
    def __init__(self, g, Z0, Wc, type:FilterElementType):
        self.g = g
        self.Z0 = Z0
        self.Wc = Wc
        self.type = type
    def componentValue(self):
        pass

    def stepedImpendenceLength(self, Vp, Z01, Z02):
        return (self.g*Vp/(self.Wc/2*pi))/2*pi

class FilterCondo(FilterElement):
    def __init__(self, g, Z0, Wc):
        super().__init__(g, Z0, Wc, FilterElementType.C)
    def componentValue(self):
        return self.g / (self.Z0 * self.Wc)

    def stepedImpendenceLength(self, Vp, Z01, Z02):
        return super().stepedImpendenceLength(Vp, Z01, Z02)*Z02/self.Z0;

class FilterInductance(FilterElement):
    def __init__(self, g, Z0, Wc):
        super().__init__(g, Z0, Wc, FilterElementType.L)
    def componentValue(self):
        return (self.g * self.Z0) / self.Wc

    def stepedImpendenceLength(self, Vp, Z01, Z02):
        return super().stepedImpendenceLength(Vp, Z01, Z02)*self.Z0/Z01


class FilterCLSeries(FilterElement):
    def __init__(self, g, Z0, Wc):
        super().__init__(g, Z0, Wc, FilterElementType.CLs)
    def componentValue(self):
        return []


class FilterCLParallel(FilterElement):
    def __init__(self, g, Z0, Wc):
        super().__init__(g, Z0, Wc, FilterElementType.CLp)


def createFilterElement(g,Z0,Wc,type:FilterElement):
    if type == FilterElementType.C:
        return FilterCondo(g, Z0, Wc)
    elif type == FilterElementType.L:
        return FilterInductance(g, Z0, Wc)
    elif type == FilterElementType.CLs:
        return FilterCLSeries(g, Z0, Wc)
    elif type == FilterElementType.CLp:
        return FilterCLParallel(g, Z0, Wc)

