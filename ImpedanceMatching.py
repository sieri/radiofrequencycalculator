from cmath import sqrt, pi

from decimal import Decimal

from matplotlib import pyplot as pp
from numpy import linspace

from Impedance import SmithPoint, Impedance, Admittance
from pySmithPlot.smithplot import SmithAxes



color = None
Z0 = Impedance(75)
w = 2*pi*1.5e+9




def add_para_c(start, value):
    points = [(Admittance(i*1j)+start).zVal() * Z0.zReal() for i in linspace(0, value)]
    pp.plot(points, label="capacitor of " + str(value), datatype=SmithAxes.Z_PARAMETER, marker='', color=color)
    c = Admittance(value*1j)
    denormalized = 1/abs(c.denormalize(Z0).to_impedance().Z*w)
    print("Adding parallel capacitor of value %.3E => %.3EF",value, denormalized)

    return c+start


def add_para_l(start, value):
    points = [(Admittance(-i*1j) + start).zVal() * Z0.zReal() for i in linspace(0, value)]
    pp.plot(points, label="capacitor of " + str(value), datatype=SmithAxes.Z_PARAMETER, marker='', color=color)
    l = Admittance(-value * 1j)
    denormalized = abs(l.denormalize(Z0).to_impedance().Z / w)

    print("Adding parallel inductance of value " + str(value) + "=>" + str(denormalized) + "H")
    return l+start


def add_serie_c(start, value):
    points = [(Impedance(-(i*1j)) + start).zVal() * Z0.zReal() for i in linspace(0, value)]
    pp.plot(points, label="capacitor of " + str(value), datatype=SmithAxes.Z_PARAMETER, marker='', color=color)

    c = Impedance(-value * 1j)
    denormalized = 1 / abs(c.denormalize(Z0).to_impedance().Z * w)
    print("Adding series capacitor of value " +  f"{Decimal(value):.2E}" + "=>" + str(denormalized) + "F")

    return c+start


def add_serie_l(start, value):
    points = [(Impedance(i * 1j) + start).zVal() * Z0.zReal() for i in linspace(0, value)]
    pp.plot(points, label="capacitor of " + str(value), datatype=SmithAxes.Z_PARAMETER, marker='', color=color)

    l = Impedance(value * 1j)
    denormalized = abs(l.denormalize(Z0).to_impedance().Z / w)

    print("Adding series inductance of value " + str(value) + "=>" + str(denormalized) + "H")
    return l+start



def add_serie_to_center(start : SmithPoint):
    value = start.zIm()
    if value > 0:
        point = add_serie_c(start, value)
    else:
        point = add_serie_l(start, -value)
    return point

def add_para_to_center(start : SmithPoint):
    value = start.yIm()
    if value > 0:
        point = add_para_l(start, value)
    else:
        point = add_para_c(start, -value)
    return point




def add_serie_component(start : SmithPoint, value):
    if value > 0:
        return add_serie_l(start, value)
    else:
        return add_serie_c(start, -value)


def add_para_component(start : SmithPoint, value):
    if value > 0:
        return add_para_c(start, value)
    else:
        return add_para_l(start, -value)


def find_combinaison(z : SmithPoint, zin = Impedance(1)):
    print("matching impedance " + str(z.to_impedance().Z))
    global color
    parallel = calc_to_imp_circle(z)

    i =0

    for x in parallel:
        if x.imag == 0:
            print("\nsolution set")
            color= str('C') + str(i)
            i = i+1
            z1 = add_para_component(z,x.real)
            z2 = add_serie_to_center(z1)
            if not (abs(z2.zReal()-1) <= 1e-5 and (z2.zIm() <= 1e-5)):
                print("ERROR: did not reach the middle point")

    serie = calc_to_adm_circle(z)

    for x in serie:
        if x.imag == 0:
            print("\nsolution set")
            color = str('C') + str(i)
            i = i + 1
            z1 = add_serie_component(z, x.real)
            z2 = add_para_to_center(z1)
            if not (abs(z2.zReal() - 1) <= 1e-5 and (z2.zIm() <= 1e-5)):
                print("ERROR: did not reach the middle point")


def calc_to_imp_circle(start : SmithPoint):
    root = sqrt(-start.yReal()**2+start.yReal())
    x = [-start.yIm() + root, -start.yIm() - root]
    return x


def calc_to_adm_circle(start : SmithPoint):
    root = sqrt(-start.zReal()**2+start.zReal())
    x = [-start.zIm() + root, -start.zIm() - root]
    return x



if __name__ == '__main__':
    z = Impedance((30-4j)).normalize(Z0)
    pp.figure(figsize=(6, 6))
    SmithAxes.update_scParams(axes_impedance=Z0.to_impedance().Z)
    ax = pp.subplot(1, 1, 1, projection='smith')
    find_combinaison(z)
    pp.show()
