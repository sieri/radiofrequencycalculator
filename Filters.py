from math import pi, sqrt

from scipy.constants.constants import speed_of_light

from FilterElement import FilterElement,FilterElementType, createFilterElement
from FilterNormalizedTables import Butterworth

f = []
order = 7
Z0 = 50
Z01 = 130
Z02 = 12

Wc = 1e9*2*pi

vpl = speed_of_light/sqrt(2.97)
vpc = speed_of_light/sqrt(4.05)

for i in range(0, order):
    if i % 2 == 0:
        f.append(createFilterElement(Butterworth[order][i], Z0, Wc, FilterElementType.C))
    else:
        f.append(createFilterElement(Butterworth[order][i], Z0, Wc, FilterElementType.L))
j = 0;
for i in f:
    if j % 2 == 0:
        print("C: " + str(i.stepedImpendenceLength(vpc, Z01, Z02) * 1000) + "mm")
    else:
        print("L: " + str(i.stepedImpendenceLength(vpl, Z01,Z02)*1000) +"mm")
    j += 1

print(vpc/(Wc/2*pi))
